package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    private final int FRIDGE_SIZE = 20;
    ArrayList<FridgeItem> items;

    public Fridge(){
        items = new ArrayList<FridgeItem>();
    }

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() { //Fridge
        return FRIDGE_SIZE;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if((totalSize()-nItemsInFridge())>0){
            items.add(item);
            return true;
        }
        else{
        return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if(items.contains(item)){
            items.remove(items.indexOf(item));
        }
        else{
            throw new NoSuchElementException("No such item in list");
        }
    }


    @Override
    public void emptyFridge() {
        items.clear();

    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredFood = new ArrayList<FridgeItem>();

        for(FridgeItem i :items) {
            if (i.hasExpired()) {
                expiredFood.add(i);
            }
        }
        for(FridgeItem e: expiredFood){
            if(items.contains(e)){
                items.remove(e);
            }
        }

        return expiredFood;
    }

}
